DROP TABLE `spa`.`sauna`;
DROP TABLE `spa`.`masaje`;
DROP TABLE `spa`.`banio`;
DROP TABLE `spa`.`drenaje`;
DROP TABLE `spa`.`paquete`;
DROP TABLE `spa`.`paquete_servicio`;

CREATE TABLE `spa`.`sauna` (
  `codigo` bigint(20) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `preciohora` float NOT NULL,
  `temperatura` float NOT NULL,
  `tiempo` float NOT NULL,
  PRIMARY KEY (`codigo`)
);
CREATE TABLE `spa`.`masaje` (
  `codigo` bigint(20) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `preciohora` float NOT NULL,
  `tipomasaje` varchar(45) NOT NULL,
  `tiempo` float NOT NULL,
  PRIMARY KEY (`codigo`)
);
CREATE TABLE `spa`.`banio` (
  `codigo` bigint(20) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `preciohora` float NOT NULL,
  `elementos` varchar(45) NOT NULL,
  `contraindicaciones` varchar(45) NOT NULL,
  PRIMARY KEY (`codigo`)
);
CREATE TABLE `spa`.`drenaje` (
  `codigo` bigint(20) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `preciohora` float NOT NULL,
  `tipodrenaje` varchar(45) NOT NULL,
  `contraindicaciones` varchar(45) NOT NULL,
  PRIMARY KEY (`codigo`)
);
CREATE TABLE `spa`.`paquete` (
  `codigo` bigint(20) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `descuento` float NOT NULL,
  PRIMARY KEY (`codigo`)
);
CREATE TABLE `spa`.`paquete_servicio` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `codigo_paquete` BIGINT NOT NULL,
  `codigo_servicio` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)
);