package ServiciosSpa.modelo;

import ServiciosSpa.modelo.views.DrenajeView;
import ServiciosSpa.persistencia.AdmPersDrenaje;

public class Drenaje extends Servicio {

	private String tipoDrenaje;
	private String contraindicaciones;
	private float precioHora;

	public Drenaje(long codigo, String nombre, float precioHora, String tipoDrenaje, String contraindicaciones) {
		this(codigo, nombre, precioHora, tipoDrenaje, contraindicaciones, false);
	}

	public Drenaje(long codigo, String nombre, float precioHora, String tipoDrenaje, String contraindicaciones, boolean pers) {
		super(codigo, nombre);
		this.precioHora = precioHora;
		this.tipoDrenaje = tipoDrenaje;
		this.contraindicaciones = contraindicaciones;

		if(!pers)
			AdmPersDrenaje.getInstancia().crear(this);
	}

	public float calcularPrecio() {
		return precioHora;
	}

	public DrenajeView getView() {
		return new DrenajeView(this);
	}

	public String tipo() {
		return "Drenaje";
	}

	public String getTipoDrenaje() {
		return tipoDrenaje;
	}

	public void setTipoDrenaje(String tipoDrenaje) {
		this.tipoDrenaje = tipoDrenaje;

		AdmPersDrenaje.getInstancia().actualizar(this);
	}

	public String getContraindicaciones() {
		return contraindicaciones;
	}

	public void setContraindicaciones(String contraindicaciones) {
		this.contraindicaciones = contraindicaciones;

		AdmPersDrenaje.getInstancia().actualizar(this);
	}

	public float getPrecioHora() {
		return precioHora;
	}

	public void setPrecioHora(float precioHora) {
		this.precioHora = precioHora;

		AdmPersDrenaje.getInstancia().actualizar(this);
	}
}
