package ServiciosSpa.modelo;

import ServiciosSpa.modelo.views.BanioView;
import ServiciosSpa.persistencia.AdmPersBanio;

public class Banio extends Servicio {

	private String elementos;
	private String contraindicaciones;
	private float precioHora;

	public Banio(long codigo, String nombre, float precioHora, String elementos, String contraindicaciones) {
		this(codigo, nombre, precioHora, elementos, contraindicaciones, false);
	}

	public Banio(long codigo, String nombre, float precioHora, String elementos, String contraindicaciones, boolean pers) {
		super(codigo, nombre);
		this.precioHora = precioHora;
		this.elementos = elementos;
		this.contraindicaciones = contraindicaciones;

		if(!pers)
			AdmPersBanio.getInstancia().crear(this);
	}

	public float calcularPrecio() {
		return precioHora;
	}

	public BanioView getView() {
		return new BanioView(this);
	}

	public String tipo() {
		return "Baño";
	}

	public String getElementos() {
		return elementos;
	}

	public void setElementos(String elementos) {
		this.elementos = elementos;

		AdmPersBanio.getInstancia().actualizar(this);
	}

	public String getContraindicaciones() {
		return contraindicaciones;
	}

	public void setContraindicaciones(String contraindicaciones) {
		this.contraindicaciones = contraindicaciones;

		AdmPersBanio.getInstancia().actualizar(this);
	}

	public float getPrecioHora() {
		return precioHora;
	}

	public void setPrecioHora(float precioHora) {
		this.precioHora = precioHora;

		AdmPersBanio.getInstancia().actualizar(this);
	}
}
