package ServiciosSpa.modelo;

import ServiciosSpa.modelo.views.ServicioView;

public abstract class Servicio {

	private long codigo;
	private String nombre;

	protected Servicio(long codigo, String nombre) {
		this.codigo = codigo;
		this.nombre = nombre;
	}

	public abstract float calcularPrecio();

	public abstract ServicioView getView();

	public boolean sosServicio(long codigo) {
		return (this.codigo == codigo);
	}

	public abstract String tipo();

	public long getCodigo() {
		return codigo;
	}

	protected void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
