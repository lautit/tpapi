package ServiciosSpa.modelo;

import ServiciosSpa.modelo.views.SaunaView;
import ServiciosSpa.persistencia.AdmPersSauna;

public class Sauna extends Servicio {

	private float temperatura;
	private float tiempo;
	private float precioHora;

	public Sauna(long codigo, String nombre, float precioHora, float temperatura, float tiempo) {
		this(codigo, nombre, precioHora, temperatura, tiempo, false);
	}

	public Sauna(long codigo, String nombre, float precioHora, float temperatura, float tiempo, boolean pers) {
		super(codigo, nombre);
		this.precioHora = precioHora;
		this.temperatura = temperatura;
		this.tiempo = tiempo;

		if(!pers)
			AdmPersSauna.getInstancia().crear(this);
	}

	public float calcularPrecio() {
		return precioHora * tiempo;
	}

	public SaunaView getView() {
		return new SaunaView(this);
	}

	public String tipo() {
		return "Sauna";
	}

	public float getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(float temperatura) {
		this.temperatura = temperatura;

		AdmPersSauna.getInstancia().actualizar(this);
	}

	public float getTiempo() {
		return tiempo;
	}

	public void setTiempo(float tiempo) {
		this.tiempo = tiempo;

		AdmPersSauna.getInstancia().actualizar(this);
	}

	public float getPrecioHora() {
		return precioHora;
	}

	public void setPrecioHora(float precioHora) {
		this.precioHora = precioHora;

		AdmPersSauna.getInstancia().actualizar(this);
	}
}
