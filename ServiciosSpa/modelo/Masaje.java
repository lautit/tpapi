package ServiciosSpa.modelo;

import ServiciosSpa.modelo.views.MasajeView;
import ServiciosSpa.persistencia.AdmPersMasaje;

public class Masaje extends Servicio {

	private String tipoMasaje;
	private float tiempo;
	private float precioHora;

	public Masaje(long codigo, String nombre, float precioHora, String tipoMasaje, float tiempo) {
		this(codigo, nombre, precioHora, tipoMasaje, tiempo, false);
	}

	public Masaje(long codigo, String nombre, float precioHora, String tipoMasaje, float tiempo, boolean pers) {
		super(codigo, nombre);
		this.precioHora = precioHora;
		this.tipoMasaje = tipoMasaje;
		this.tiempo = tiempo;

		if(!pers)
			AdmPersMasaje.getInstancia().crear(this);
	}

	public float calcularPrecio() {
		return precioHora * tiempo;
	}

	public MasajeView getView() {
		return new MasajeView(this);
	}

	public String tipo() {
		return "Masaje";
	}

	public String getTipoMasaje() {
		return tipoMasaje;
	}

	public void setTipoMasaje(String tipoMasaje) {
		this.tipoMasaje = tipoMasaje;

		AdmPersMasaje.getInstancia().actualizar(this);
	}

	public float getTiempo() {
		return tiempo;
	}

	public void setTiempo(float tiempo) {
		this.tiempo = tiempo;

		AdmPersMasaje.getInstancia().actualizar(this);
	}

	public float getPrecioHora() {
		return precioHora;
	}

	public void setPrecioHora(float precioHora) {
		this.precioHora = precioHora;

		AdmPersMasaje.getInstancia().actualizar(this);
	}
}
