package ServiciosSpa.modelo;

import ServiciosSpa.modelo.views.PaqueteView;
import ServiciosSpa.modelo.views.ServicioView;
import ServiciosSpa.persistencia.AdmPersPaquete;

import java.util.ArrayList;
import java.util.List;

public class Paquete extends Servicio {

	private float descuento;
	private List<Servicio> servicios;

	public Paquete(long codigo, String nombre, float descuento, List<Servicio> servicios) {
		this(codigo, nombre, descuento, servicios, false);
	}

	public Paquete(long codigo, String nombre, float descuento, List<Servicio> servicios, boolean pers) {
		super(codigo, nombre);
		this.servicios = servicios;
		this.descuento = descuento;

		if(!pers)
			AdmPersPaquete.getInstancia().crear(this);
	}

	public float calcularPrecio() {
		float precio = 0;

		for(Servicio servicio : servicios) {
			precio += servicio.calcularPrecio();
		}

		return precio * (1 - descuento);
	}

	public PaqueteView getView() {
		return new PaqueteView(this);
	}

	public String tipo() {
		return "Paquete";
	}

	public float getDescuento() {
		return descuento;
	}

	public void setDescuento(float descuento) {
		this.descuento = descuento;

		AdmPersPaquete.getInstancia().actualizar(this);
	}

	public List<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(List<Servicio> servicios) {
		this.servicios = servicios;

		AdmPersPaquete.getInstancia().actualizar(this);
	}

	public void agregarServicio(Servicio servicio) {
		this.servicios.add(servicio);

		AdmPersPaquete.getInstancia().actualizar(this);
	}

	public void quitarServicio(Servicio servicio) {
		this.servicios.remove(servicio);

		AdmPersPaquete.getInstancia().actualizar(this);
	}

    public List<ServicioView> getServiciosViews() {
		List<ServicioView> views = new ArrayList<ServicioView>();

		for(Servicio s : this.servicios) {
			views.add(s.getView());
		}

		return views;
    }
}
