package ServiciosSpa.modelo.views;

import ServiciosSpa.modelo.Banio;

public class BanioView extends ServicioView {

	private String elementos;
	private String contraindicaciones;
	private float precioHora;

	public BanioView(Banio b) {
		super(b);
		this.elementos = b.getElementos();
		this.contraindicaciones = b.getContraindicaciones();
		this.precioHora = b.getPrecioHora();
	}

	public String getElementos() {
		return elementos;
	}

	public void setElementos(String elementos) {
		this.elementos = elementos;
	}

	public String getContraindicaciones() {
		return contraindicaciones;
	}

	public void setContraindicaciones(String contraindicaciones) {
		this.contraindicaciones = contraindicaciones;
	}

	public float getPrecioHora() {
		return precioHora;
	}

	public void setPrecioHora(float precioHora) {
		this.precioHora = precioHora;
	}
}
