package ServiciosSpa.modelo.views;

import ServiciosSpa.modelo.Masaje;

public class MasajeView extends ServicioView {

	private String tipoMasaje;
	private float tiempo;
	private float precioHora;

	public MasajeView(Masaje m) {
		super(m);
		this.tipoMasaje = m.getTipoMasaje();
		this.tiempo = m.getTiempo();
		this.precioHora = m.getPrecioHora();
	}

	public String getTipoMasaje() {
		return tipoMasaje;
	}

	public void setTipoMasaje(String tipoMasaje) {
		this.tipoMasaje = tipoMasaje;
	}

	public float getTiempo() {
		return tiempo;
	}

	public void setTiempo(float tiempo) {
		this.tiempo = tiempo;
	}

	public float getPrecioHora() {
		return precioHora;
	}

	public void setPrecioHora(float precioHora) {
		this.precioHora = precioHora;
	}
}
