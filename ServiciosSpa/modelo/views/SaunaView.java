package ServiciosSpa.modelo.views;

import ServiciosSpa.modelo.Sauna;

public class SaunaView extends ServicioView {

	private float temperatura;
	private float tiempo;
	private float precioHora;

	public SaunaView(Sauna s) {
		super(s);
		this.temperatura = s.getTemperatura();
		this.tiempo = s.getTiempo();
		this.precioHora = s.getPrecioHora();
	}

	public float getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(float temperatura) {
		this.temperatura = temperatura;
	}

	public float getTiempo() {
		return tiempo;
	}

	public void setTiempo(float tiempo) {
		this.tiempo = tiempo;
	}

	public float getPrecioHora() {
		return precioHora;
	}

	public void setPrecioHora(float precioHora) {
		this.precioHora = precioHora;
	}
}
