package ServiciosSpa.modelo.views;

import ServiciosSpa.modelo.Paquete;

import java.util.List;

public class PaqueteView extends ServicioView {

	private float descuento;
	private List<ServicioView> serviciosView;

	public PaqueteView(Paquete s) {
		super(s);
		this.descuento = s.getDescuento();
		this.serviciosView = s.getServiciosViews();
	}

	public float getDescuento() {
		return descuento;
	}

	public void setDescuento(float descuento) {
		this.descuento = descuento;
	}

	public List<ServicioView> getServiciosView() {
		return serviciosView;
	}

	public void setServiciosView(List<ServicioView> serviciosView) {
		this.serviciosView = serviciosView;
	}
}
