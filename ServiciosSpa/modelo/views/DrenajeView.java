package ServiciosSpa.modelo.views;

import ServiciosSpa.modelo.Drenaje;

public class DrenajeView extends ServicioView {

	private String tipoDrenaje;
	private String contraindicaciones;
	private float precioHora;

	public DrenajeView(Drenaje d) {
		super(d);
		this.tipoDrenaje = d.getContraindicaciones();
		this.contraindicaciones = d.getContraindicaciones();
		this.precioHora = d.getPrecioHora();
	}

	public String getTipoDrenaje() {
		return tipoDrenaje;
	}

	public void setTipoDrenaje(String tipoDrenaje) {
		this.tipoDrenaje = tipoDrenaje;
	}

	public String getContraindicaciones() {
		return contraindicaciones;
	}

	public void setContraindicaciones(String contraindicaciones) {
		this.contraindicaciones = contraindicaciones;
	}

	public float getPrecioHora() {
		return precioHora;
	}

	public void setPrecioHora(float precioHora) {
		this.precioHora = precioHora;
	}
}
