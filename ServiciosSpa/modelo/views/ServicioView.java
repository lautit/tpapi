package ServiciosSpa.modelo.views;

import ServiciosSpa.modelo.Servicio;

public abstract class ServicioView {

	private long codigo;
	private String nombre;
	private float precio;

	protected ServicioView(Servicio s) {
		this.codigo = s.getCodigo();
		this.nombre = s.getNombre();
		this.precio = s.calcularPrecio();
	}

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	@Override
	public String toString() {
		return codigo + " - " + nombre + " $" + precio;
	}

	@Override
	public boolean equals(Object obj) {
		ServicioView s = (ServicioView) obj;

		return (this.codigo == s.getCodigo());
	}
}
