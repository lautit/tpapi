package ServiciosSpa.vista;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Pantalla extends JFrame {

	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu menuServicios;
	private JMenu menuCrear, menuModificar;
	private JMenuItem menuItemCrearSauna, menuItemCrearMasaje, menuItemCrearBanios,
			menuItemCrearDrenaje, menuItemCrearPaquete, menuItemModificarSauna,
			menuItemModificarMasaje, menuItemModificarBanios, menuItemModificarDrenaje,
			menuItemModificarPaquete, menuItemEliminar;
	private JButton btnSalir;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
            try {
                Pantalla frame = new Pantalla();
                frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
	}

	public Pantalla() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(20, 20, 900, 600);
		setResizable(false);

		addWindowListener((new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		}));

		initMenu();
		
		btnSalir = new JButton("Salir");
		btnSalir.addActionListener(actionEvent -> System.exit(0));
		menuBar.add(btnSalir);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

	private void initMenu() {
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		menuServicios = new JMenu("Servicios");
		menuBar.add(menuServicios);

		initMenuCrear();
		initMenuModificar();
		initMenuEliminar();
	}

	private void initMenuCrear() {
		menuCrear = new JMenu("Crear");
		menuServicios.add(menuCrear);

		menuItemCrearSauna = new JMenuItem("Sauna");
		menuItemCrearSauna.addActionListener(actionEvent -> {
			AltaSauna alta = new AltaSauna();
			alta.setVisible(true);
		});
		menuCrear.add(menuItemCrearSauna);

		menuItemCrearMasaje = new JMenuItem("Masajes");
		menuItemCrearMasaje.addActionListener(actionEvent -> {
			AltaMasajes alta = new AltaMasajes();
			alta.setVisible(true);
		});
		menuCrear.add(menuItemCrearMasaje);

		menuItemCrearBanios = new JMenuItem("Ba\u00F1os");
		menuItemCrearBanios.addActionListener(actionEvent -> {
			AltaBanio alta = new AltaBanio();
			alta.setVisible(true);
		});
		menuCrear.add(menuItemCrearBanios);

		menuItemCrearDrenaje = new JMenuItem("Drenaje");
		menuItemCrearDrenaje.addActionListener(actionEvent -> {
			AltaDrenaje alta = new AltaDrenaje();
			alta.setVisible(true);
		});
		menuCrear.add(menuItemCrearDrenaje);

		menuItemCrearPaquete = new JMenuItem("Paquete");
		menuItemCrearPaquete.addActionListener(actionEvent -> {
            AltaPaquete alta = new AltaPaquete();
            alta.setVisible(true);
        });
		menuCrear.add(menuItemCrearPaquete);
	}

	private void initMenuModificar() {
		menuModificar = new JMenu("Modificar");
		menuServicios.add(menuModificar);

		menuItemModificarSauna = new JMenuItem("Sauna");
		menuItemModificarSauna.addActionListener(actionEvent -> {
			ModificarSauna alta = new ModificarSauna();
			alta.setVisible(true);
		});
		menuModificar.add(menuItemModificarSauna);

		menuItemModificarMasaje = new JMenuItem("Masajes");
		menuItemModificarMasaje.addActionListener(actionEvent -> {
			ModificarMasajes alta = new ModificarMasajes();
			alta.setVisible(true);
		});
		menuModificar.add(menuItemModificarMasaje);

		menuItemModificarBanios = new JMenuItem("Ba\u00F1os");
		menuItemModificarBanios.addActionListener(actionEvent -> {
			ModificarBanios alta = new ModificarBanios();
			alta.setVisible(true);
		});
		menuModificar.add(menuItemModificarBanios);

		menuItemModificarDrenaje = new JMenuItem("Drenaje");
		menuItemModificarDrenaje.addActionListener(actionEvent -> {
			ModificarDrenaje alta = new ModificarDrenaje();
			alta.setVisible(true);
		});
		menuModificar.add(menuItemModificarDrenaje);

		menuItemModificarPaquete = new JMenuItem("Paquete");
		menuItemModificarPaquete.addActionListener(actionEvent -> {
			ModificarPaquete alta = new ModificarPaquete();
			alta.setVisible(true);
		});
		menuModificar.add(menuItemModificarPaquete);
	}

	private void initMenuEliminar() {
		menuItemEliminar = new JMenuItem("Eliminar");
		menuItemEliminar.addActionListener(actionEvent -> {
			BajaServicio baja= new BajaServicio();
			baja.setVisible(true);
		});
		menuServicios.add(menuItemEliminar);
	}

}
