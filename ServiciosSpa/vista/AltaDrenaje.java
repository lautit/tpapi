package ServiciosSpa.vista;

import ServiciosSpa.controlador.SistAdminServicios;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class AltaDrenaje extends JFrame {

	private JPanel contentPane;
	private JTextField nombre;
	private JTextField precioxhora;
	private JTextField tipo;
	private JTextField contraindicaciones;

	public AltaDrenaje() {
		setTitle("Servicio de Drenaje");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		JButton btnSalir = new JButton("Cancelar");
		btnSalir.addActionListener(actionEvent -> dispose());
		contentPane.add(btnSalir, "18, 2");
		
		JLabel lblNewLabel = new JLabel("Nombre");
		contentPane.add(lblNewLabel, "6, 4");
		
		JLabel lblNewLabel_1 = new JLabel("Precio por hora");
		contentPane.add(lblNewLabel_1, "14, 4");
		
		nombre = new JTextField();
		contentPane.add(nombre, "6, 6, fill, default");
		nombre.setColumns(10);
		
		precioxhora = new JTextField();
		contentPane.add(precioxhora, "14, 6, fill, default");
		precioxhora.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Tipo");
		contentPane.add(lblNewLabel_2, "6, 12");
		
		JLabel lblNewLabel_3 = new JLabel("Contraindicaciones");
		contentPane.add(lblNewLabel_3, "14, 12");
		
		tipo = new JTextField();
		contentPane.add(tipo, "6, 14, fill, default");
		tipo.setColumns(10);
		
		contraindicaciones = new JTextField();
		contentPane.add(contraindicaciones, "14, 14, 4, 3, fill, default");
		contraindicaciones.setColumns(10);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(actionEvent -> {
            SistAdminServicios.getInstancia().crearServicioDrenaje(nombre.getText(), Float.parseFloat(precioxhora.getText()), tipo.getText(), contraindicaciones.getText());
            nombre.setText("");
            precioxhora.setText("");
            tipo.setText("");
            contraindicaciones.setText("");
        });
		contentPane.add(btnGuardar, "6, 20");
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(actionEvent -> {
            nombre.setText("");
            precioxhora.setText("");
            tipo.setText("");
            contraindicaciones.setText("");
        });
		contentPane.add(btnLimpiar, "14, 20");
	}

}
