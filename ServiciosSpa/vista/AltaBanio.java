package ServiciosSpa.vista;

import ServiciosSpa.controlador.SistAdminServicios;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class AltaBanio extends JFrame {

	private JPanel contentPane;
	private JTextField nombre;
	private JTextField precioxhora;
	private JTextField elementos;
	private JTextField contraindicaciones;

	public AltaBanio() {
		setTitle("Servicio de Ba\u00F1o");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(actionEvent -> dispose());
		GridBagConstraints gbc_btnCancelar = new GridBagConstraints();
		gbc_btnCancelar.anchor = GridBagConstraints.EAST;
		gbc_btnCancelar.insets = new Insets(0, 0, 5, 0);
		gbc_btnCancelar.gridx = 9;
		gbc_btnCancelar.gridy = 0;
		contentPane.add(btnCancelar, gbc_btnCancelar);
		
		JLabel lblNewLabel = new JLabel("Nombre:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 1;
		contentPane.add(lblNewLabel, gbc_lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Precio por hora");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 6;
		gbc_lblNewLabel_1.gridy = 1;
		contentPane.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		nombre = new JTextField();
		GridBagConstraints gbc_nombre = new GridBagConstraints();
		gbc_nombre.gridwidth = 2;
		gbc_nombre.insets = new Insets(0, 0, 5, 5);
		gbc_nombre.fill = GridBagConstraints.HORIZONTAL;
		gbc_nombre.gridx = 1;
		gbc_nombre.gridy = 2;
		contentPane.add(nombre, gbc_nombre);
		nombre.setColumns(10);
		
		precioxhora = new JTextField();
		GridBagConstraints gbc_precioxhora = new GridBagConstraints();
		gbc_precioxhora.insets = new Insets(0, 0, 5, 5);
		gbc_precioxhora.fill = GridBagConstraints.HORIZONTAL;
		gbc_precioxhora.gridx = 6;
		gbc_precioxhora.gridy = 2;
		contentPane.add(precioxhora, gbc_precioxhora);
		precioxhora.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Elementos Utilizados");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.gridwidth = 3;
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 1;
		gbc_lblNewLabel_2.gridy = 4;
		contentPane.add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Contraindicaciones");
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_3.gridwidth = 2;
		gbc_lblNewLabel_3.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_3.gridx = 6;
		gbc_lblNewLabel_3.gridy = 4;
		contentPane.add(lblNewLabel_3, gbc_lblNewLabel_3);
		
		elementos = new JTextField();
		GridBagConstraints gbc_elementos = new GridBagConstraints();
		gbc_elementos.gridheight = 2;
		gbc_elementos.gridwidth = 3;
		gbc_elementos.insets = new Insets(0, 0, 5, 5);
		gbc_elementos.fill = GridBagConstraints.BOTH;
		gbc_elementos.gridx = 1;
		gbc_elementos.gridy = 5;
		contentPane.add(elementos, gbc_elementos);
		elementos.setColumns(10);
		
		contraindicaciones = new JTextField();
		GridBagConstraints gbc_contraindicaciones = new GridBagConstraints();
		gbc_contraindicaciones.gridheight = 2;
		gbc_contraindicaciones.gridwidth = 2;
		gbc_contraindicaciones.insets = new Insets(0, 0, 5, 5);
		gbc_contraindicaciones.fill = GridBagConstraints.BOTH;
		gbc_contraindicaciones.gridx = 6;
		gbc_contraindicaciones.gridy = 5;
		contentPane.add(contraindicaciones, gbc_contraindicaciones);
		contraindicaciones.setColumns(10);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(actionEvent -> {
            SistAdminServicios.getInstancia().crearServicioBanios(nombre.getText(), Float.parseFloat(precioxhora.getText()), elementos.getText(), contraindicaciones.getText() );
            nombre.setText("");
            precioxhora.setText("");
            elementos.setText("");
            contraindicaciones.setText("");
        });
		GridBagConstraints gbc_btnGuardar = new GridBagConstraints();
		gbc_btnGuardar.gridwidth = 2;
		gbc_btnGuardar.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnGuardar.insets = new Insets(0, 0, 0, 5);
		gbc_btnGuardar.gridx = 1;
		gbc_btnGuardar.gridy = 8;
		contentPane.add(btnGuardar, gbc_btnGuardar);
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(actionEvent -> {
            nombre.setText("");
            precioxhora.setText("");
            elementos.setText("");
            contraindicaciones.setText("");
        });
		GridBagConstraints gbc_btnLimpiar = new GridBagConstraints();
		gbc_btnLimpiar.insets = new Insets(0, 0, 0, 5);
		gbc_btnLimpiar.gridx = 7;
		gbc_btnLimpiar.gridy = 8;
		contentPane.add(btnLimpiar, gbc_btnLimpiar);
	}

}
