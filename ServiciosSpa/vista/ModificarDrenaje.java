package ServiciosSpa.vista;

import ServiciosSpa.controlador.SistAdminServicios;
import ServiciosSpa.modelo.views.DrenajeView;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class ModificarDrenaje extends JFrame {

	private JPanel contentPane;
	private JTextField codigo;
	private JTextField precioporhora;
	private JTextField nombre;
	private JTextField tipo_drenaje;
	private JTextField contraindicaciones;

	
	public ModificarDrenaje() {
		setTitle("Modificar Drenaje");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(63, 35, 33, 14);
		contentPane.add(lblCodigo);
		
		JLabel lblPrecioPorHora = new JLabel("Precio por hora");
		lblPrecioPorHora.setBounds(42, 84, 75, 14);
		contentPane.add(lblPrecioPorHora);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(61, 133, 37, 14);
		contentPane.add(lblNombre);
		
		JLabel lblTipoDeDrenaje = new JLabel("Tipo de Drenaje");
		lblTipoDeDrenaje.setBounds(42, 182, 76, 14);
		contentPane.add(lblTipoDeDrenaje);
		
		JLabel lblContraindicaciones = new JLabel("Contraindicaciones");
		lblContraindicaciones.setBounds(35, 231, 90, 14);
		contentPane.add(lblContraindicaciones);
		
		codigo = new JTextField();
		codigo.setBounds(139, 32, 86, 20);
		contentPane.add(codigo);
		codigo.setColumns(10);

		precioporhora = new JTextField();
		precioporhora.setBounds(139, 81, 86, 20);
		contentPane.add(precioporhora);
		precioporhora.setColumns(10);

		nombre = new JTextField();
		nombre.setBounds(139, 130, 86, 20);
		contentPane.add(nombre);
		nombre.setColumns(10);
		
		tipo_drenaje = new JTextField();
		tipo_drenaje.setBounds(139, 179, 86, 20);
		contentPane.add(tipo_drenaje);
		tipo_drenaje.setColumns(10);
		
		contraindicaciones = new JTextField();
		contraindicaciones.setBounds(139, 228, 86, 20);
		contentPane.add(contraindicaciones);
		contraindicaciones.setColumns(10);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(actionEvent -> {
            DrenajeView d = SistAdminServicios.getInstancia().editarServicioDrenaje(Long.parseLong(codigo.getText()));
            nombre.setText(d.getNombre());
            precioporhora.setText(String.valueOf(d.getPrecioHora()));
            tipo_drenaje.setText(d.getTipoDrenaje());
            contraindicaciones.setText(d.getContraindicaciones());
        });
		btnBuscar.setBounds(235, 31, 89, 23);
		contentPane.add(btnBuscar);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(actionEvent -> SistAdminServicios.getInstancia().editarServicioDrenaje(
                Long.parseLong(codigo.getText()),
                nombre.getText(),
                Float.parseFloat(precioporhora.getText()),
                tipo_drenaje.getText(),
                contraindicaciones.getText()
        ));
		btnGuardar.setBounds(308, 100, 89, 23);
		contentPane.add(btnGuardar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(actionEvent -> dispose());
		btnCancelar.setBounds(308, 160, 89, 23);
		contentPane.add(btnCancelar);
	}

}
