package ServiciosSpa.vista;

import ServiciosSpa.controlador.SistAdminServicios;
import ServiciosSpa.modelo.views.PaqueteView;
import ServiciosSpa.modelo.views.ServicioView;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class ModificarPaquete extends JFrame {

	private JPanel contentPane;
	private JTextField nombre, descuento, codigo;
	private JList<ServicioView> listaServicios;
	private DefaultListModel<ServicioView> model;
	private JButton btnBuscar, btnGuardar, btnCancelar, btnSeleccionarTodos, btnDeseleccionarTodos;
	private JLabel lblNombre, lblDescuento, lblServicios, lblCodigo;

	public ModificarPaquete() {
		setTitle("Modificar Paquete");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 350);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		crearLista();
		crearLabelsFields();
		crearButtons();
	}

	private void crearLabelsFields() {
		lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(30, 10, 70, 30);
		contentPane.add(lblCodigo);

		codigo = new JTextField();
		codigo.setBounds(110, 10, 100, 30);
		contentPane.add(codigo);
		codigo.setColumns(10);

		lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(30, 50, 70, 30);
		contentPane.add(lblNombre);

		nombre = new JTextField();
		nombre.setBounds(110, 50, 100, 30);
		contentPane.add(nombre);
		nombre.setColumns(10);

		lblDescuento = new JLabel("Descuento");
		lblDescuento.setBounds(220, 50, 70, 30);
		contentPane.add(lblDescuento);

		descuento = new JTextField();
		descuento.setBounds(300, 50, 100, 30);
		contentPane.add(descuento);
		descuento.setColumns(10);

		lblServicios = new JLabel("Servicios:");
		lblServicios.setBounds(30, 90, 70, 30);
		contentPane.add(lblServicios);
	}

	private void crearButtons() {
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(actionEvent -> {
			SistAdminServicios.getInstancia().crearServicioPaquete(
					nombre.getText(),
					Float.parseFloat(descuento.getText()),
					listaServicios.getSelectedValuesList()
			);

			nombre.setText("");
			descuento.setText("");
			listaServicios.clearSelection();
		});
		btnGuardar.setBounds(50, 260, 100, 30);
		contentPane.add(btnGuardar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnCancelar.setBounds(200, 260, 100, 30);
		contentPane.add(btnCancelar);

		btnSeleccionarTodos = new JButton("Seleccionar todos");
		btnSeleccionarTodos.addActionListener(actionEvent -> {
			listaServicios.setSelectionInterval(0, model.getSize() - 1);
		});
		btnSeleccionarTodos.setBounds(290, 130, 200, 30);
		contentPane.add(btnSeleccionarTodos);

		btnDeseleccionarTodos = new JButton("Deseleccionar todos");
		btnDeseleccionarTodos.addActionListener(actionEvent -> {
			listaServicios.clearSelection();
		});
		btnDeseleccionarTodos.setBounds(290, 170, 200, 30);
		contentPane.add(btnDeseleccionarTodos);

		btnBuscar = new JButton("Buscar Paquete");
		btnBuscar.addActionListener(actionEvent -> {
			PaqueteView paqueteView = SistAdminServicios.getInstancia().editarServicioPaquete(Long.parseLong(codigo.getText()));
			nombre.setText(paqueteView.getNombre());
			descuento.setText(String.valueOf(paqueteView.getDescuento()));

			ArrayList<Integer> elementos = new ArrayList<>();
			for(ServicioView servicioView : paqueteView.getServiciosView()) {
				elementos.add(model.indexOf(servicioView));
			}
			int[] seleccionar = new int[elementos.size()];
			for(int i = 0; i < elementos.size(); i++) {
				seleccionar[i] = elementos.get(i);
			}
			listaServicios.setSelectedIndices(seleccionar);
		});
		btnBuscar.setBounds(220, 10, 170, 30);
		contentPane.add(btnBuscar);
	}

	private void crearLista() {
		listaServicios = new JList<>();
		model = new DefaultListModel<>();
		listaServicios.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		List<ServicioView> todos = SistAdminServicios.getInstancia().listarServicios();
		for(ServicioView servicioView : todos)
			model.addElement(servicioView);
		listaServicios.setModel(model);
		listaServicios.setBounds(30, 130, 220, 120);
		contentPane.add(listaServicios);
	}
}