package ServiciosSpa.vista;

import ServiciosSpa.controlador.SistAdminServicios;
import ServiciosSpa.modelo.views.SaunaView;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class ModificarSauna extends JFrame {

	private JPanel contentPane;
	private JTextField codigo;
	private JTextField precioporhora;
	private JTextField nombre;
	private JTextField temperatura;
	private JTextField tiempo_exp;
	private JButton btnBuscar;
	private JButton btnGuardar;
	private JButton btnCancelar;

	
	public ModificarSauna() {
		setTitle("Modificar Sauna");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(36, 36, 90, 14);
		contentPane.add(lblCodigo);
		
		JLabel lblPrecioPorHora = new JLabel("Precio por hora");
		lblPrecioPorHora.setBounds(36, 81, 90, 14);
		contentPane.add(lblPrecioPorHora);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(36, 126, 90, 14);
		contentPane.add(lblNombre);
		
		JLabel lblTemperatura = new JLabel("Temperatura");
		lblTemperatura.setBounds(36, 171, 90, 14);
		contentPane.add(lblTemperatura);
		
		JLabel lblTiempoDeExposicion = new JLabel("Tiempo de Exposición");
		lblTiempoDeExposicion.setBounds(36, 216, 90, 14);
		contentPane.add(lblTiempoDeExposicion);
		
		codigo = new JTextField();
		codigo.setBounds(136, 33, 86, 20);
		contentPane.add(codigo);
		codigo.setColumns(10);
		
		precioporhora = new JTextField();
		precioporhora.setBounds(136, 78, 86, 20);
		contentPane.add(precioporhora);
		precioporhora.setColumns(10);
		
		nombre = new JTextField();
		nombre.setBounds(136, 123, 86, 20);
		contentPane.add(nombre);
		nombre.setColumns(10);
		
		temperatura = new JTextField();
		temperatura.setBounds(136, 168, 86, 20);
		contentPane.add(temperatura);
		temperatura.setColumns(10);
		
		tiempo_exp = new JTextField();
		tiempo_exp.setBounds(136, 213, 86, 20);
		contentPane.add(tiempo_exp);
		tiempo_exp.setColumns(10);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(actionEvent -> {
            SaunaView s = SistAdminServicios.getInstancia().editarServicioSauna(Long.parseLong(codigo.getText()));
            nombre.setText(s.getNombre());
            precioporhora.setText(String.valueOf(s.getPrecioHora()));
            temperatura.setText(String.valueOf(s.getTemperatura()));
            tiempo_exp.setText(String.valueOf(s.getTiempo()));
        });
		btnBuscar.setBounds(232, 32, 89, 23);
		contentPane.add(btnBuscar);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(actionEvent -> SistAdminServicios.getInstancia().editarServicioSauna(
                Long.parseLong(codigo.getText()),
                nombre.getText(),
                Float.parseFloat(precioporhora.getText()),
                Float.parseFloat(temperatura.getText()),
                Float.parseFloat(tiempo_exp.getText())
        ));
		btnGuardar.setBounds(312, 94, 89, 23);
		contentPane.add(btnGuardar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(actionEvent -> dispose());
		btnCancelar.setBounds(312, 167, 89, 23);
		contentPane.add(btnCancelar);
	}

}
