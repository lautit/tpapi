package ServiciosSpa.vista;

import ServiciosSpa.controlador.SistAdminServicios;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class AltaSauna extends JFrame {

	private JPanel contentPane;
	private JTextField nombre;
	private JTextField temperatura;
	private JTextField tiempo;
	private JButton btnGuardar;
	private JButton btnLimpiar;
	private JButton btnSalir;
	private JTextField precioxhora;

	public AltaSauna() {
		setTitle("Servicio de Sauna");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		btnSalir = new JButton("Cancelar");
		btnSalir.setBounds(320, 14, 75, 23);
		btnSalir.addActionListener(actionEvent -> dispose());
		contentPane.setLayout(null);
		contentPane.add(btnSalir);
		
		JLabel lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setBounds(35, 42, 62, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Precio por hora");
		lblNewLabel_1.setBounds(194, 42, 73, 14);
		contentPane.add(lblNewLabel_1);
		
		nombre = new JTextField();
		nombre.setBounds(35, 61, 62, 20);
		contentPane.add(nombre);
		nombre.setColumns(10);
		
		precioxhora = new JTextField();
		precioxhora.setBounds(194, 61, 91, 20);
		contentPane.add(precioxhora);
		precioxhora.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Temperatura");
		lblNewLabel_2.setBounds(35, 116, 62, 14);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Tiempo de Exposicion");
		lblNewLabel_3.setBounds(194, 116, 116, 14);
		contentPane.add(lblNewLabel_3);
		
		temperatura = new JTextField();
		temperatura.setBounds(35, 135, 62, 20);
		contentPane.add(temperatura);
		temperatura.setColumns(10);
		
		tiempo = new JTextField();
		tiempo.setBounds(204, 135, 61, 20);
		contentPane.add(tiempo);
		tiempo.setColumns(10);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(37, 220, 71, 23);
		btnGuardar.addActionListener(actionEvent -> {
            SistAdminServicios.getInstancia().crearServicioSauna(
                    nombre.getText(),
                    Float.parseFloat(precioxhora.getText()),
                    Float.parseFloat(temperatura.getText()),
                    Float.parseFloat(tiempo.getText())
            );
            nombre.setText("");
            precioxhora.setText("");
            tiempo.setText("");
            temperatura.setText("");
        });
		contentPane.add(btnGuardar);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.setBounds(310, 220, 65, 23);
		btnLimpiar.addActionListener(actionEvent -> {
            nombre.setText("");
            temperatura.setText("");
            tiempo.setText("");
        });
		contentPane.add(btnLimpiar);
	}

}
