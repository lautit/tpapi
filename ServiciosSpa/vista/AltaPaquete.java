package ServiciosSpa.vista;

import ServiciosSpa.controlador.SistAdminServicios;
import ServiciosSpa.modelo.views.ServicioView;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class AltaPaquete extends JFrame {

	private JPanel contentPane;
	private JTextField nombre, descuento;
	private JList<ServicioView> listaServicios;
	private DefaultListModel<ServicioView> model;
	private JButton btnGuardar, btnCancelar, btnSeleccionarTodos, btnDeseleccionarTodos;
	private JLabel lblNombre, lblDescuento, lblServicios;

	public AltaPaquete() {
		setTitle("Paquete de Servicios");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		setBounds(100, 100, 550, 350);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		crearLista();
		crearLabelsFields();
		crearButtons();
	}

	private void crearLabelsFields() {
		lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(30, 10, 70, 30);
		contentPane.add(lblNombre);

		lblDescuento = new JLabel("Descuento");
		lblDescuento.setBounds(30, 50, 70, 30);
		contentPane.add(lblDescuento);

		lblServicios = new JLabel("Servicios:");
		lblServicios.setBounds(30, 90, 70, 30);
		contentPane.add(lblServicios);

		nombre = new JTextField();
		nombre.setBounds(100, 10, 100, 30);
		contentPane.add(nombre);
		nombre.setColumns(10);

		descuento = new JTextField();
		descuento.setBounds(100, 50, 100, 30);
		contentPane.add(descuento);
		descuento.setColumns(10);
	}

	private void crearButtons() {
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(actionEvent -> {
			SistAdminServicios.getInstancia().crearServicioPaquete(
					nombre.getText(),
					Float.parseFloat(descuento.getText()),
					listaServicios.getSelectedValuesList()
			);

			nombre.setText("");
			descuento.setText("");
			listaServicios.clearSelection();
		});
		btnGuardar.setBounds(50, 260, 100, 30);
		contentPane.add(btnGuardar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnCancelar.setBounds(200, 260, 100, 30);
		contentPane.add(btnCancelar);

		btnSeleccionarTodos = new JButton("Seleccionar todos");
		btnSeleccionarTodos.addActionListener(actionEvent -> {
			listaServicios.setSelectionInterval(0, model.getSize() - 1);
		});
		btnSeleccionarTodos.setBounds(290, 130, 200, 30);
		contentPane.add(btnSeleccionarTodos);

		btnDeseleccionarTodos = new JButton("Deseleccionar todos");
		btnDeseleccionarTodos.addActionListener(actionEvent -> {
			listaServicios.clearSelection();
		});
		btnDeseleccionarTodos.setBounds(290, 170, 200, 30);
		contentPane.add(btnDeseleccionarTodos);
	}

	private void crearLista() {
		listaServicios = new JList<>();
		model = new DefaultListModel<>();
		listaServicios.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		List<ServicioView> todos = SistAdminServicios.getInstancia().listarServicios();
		for(ServicioView servicioView : todos)
			model.addElement(servicioView);
		listaServicios.setModel(model);
		listaServicios.setBounds(30, 130, 220, 120);
		contentPane.add(listaServicios);
	}
}
