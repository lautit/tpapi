package ServiciosSpa.vista;

import ServiciosSpa.controlador.SistAdminServicios;
import ServiciosSpa.modelo.views.MasajeView;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class ModificarMasajes extends JFrame {

	private JPanel contentPane;
	private JTextField codigo;
	private JTextField precioporhora;
	private JTextField nombre;
	private JTextField tipo_masaje;
	private JTextField tiempo_exp;

	
	public ModificarMasajes() {
		setTitle("Modificar Masajes");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(10, 38, 33, 14);
		contentPane.add(lblCodigo);
		
		JLabel lblPrecioPorHora = new JLabel("Precio por hora");
		lblPrecioPorHora.setBounds(10, 87, 75, 14);
		contentPane.add(lblPrecioPorHora);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 139, 37, 14);
		contentPane.add(lblNombre);
		
		JLabel lblTipoDeMasaje = new JLabel("Tipo de Masaje");
		lblTipoDeMasaje.setBounds(13, 184, 72, 17);
		contentPane.add(lblTipoDeMasaje);
		
		JLabel lblTiempoDeExposicion = new JLabel("Tiempo de exposicion");
		lblTiempoDeExposicion.setBounds(10, 234, 102, 14);
		contentPane.add(lblTiempoDeExposicion);
		
		codigo = new JTextField();
		codigo.setBounds(161, 35, 86, 20);
		contentPane.add(codigo);
		codigo.setColumns(10);
		
		precioporhora = new JTextField();
		precioporhora.setBounds(161, 84, 86, 20);
		contentPane.add(precioporhora);
		precioporhora.setColumns(10);
		
		nombre = new JTextField();
		nombre.setBounds(161, 136, 86, 20);
		contentPane.add(nombre);
		nombre.setColumns(10);
		
		tipo_masaje = new JTextField();
		tipo_masaje.setBounds(161, 182, 86, 20);
		contentPane.add(tipo_masaje);
		tipo_masaje.setColumns(10);
		
		tiempo_exp = new JTextField();
		tiempo_exp.setBounds(161, 231, 86, 20);
		contentPane.add(tiempo_exp);
		tiempo_exp.setColumns(10);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(actionEvent -> {
            MasajeView m = SistAdminServicios.getInstancia().editarServicioMasaje(Long.parseLong(codigo.getText()));
            nombre.setText(m.getNombre());
            precioporhora.setText(String.valueOf(m.getPrecioHora()));
            tipo_masaje.setText(m.getTipoMasaje());
            tiempo_exp.setText(String.valueOf(m.getTiempo()));
        });
		btnBuscar.setBounds(253, 34, 89, 23);
		contentPane.add(btnBuscar);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(actionEvent -> SistAdminServicios.getInstancia().editarServicioMasaje(
                Long.parseLong(codigo.getText()),
                nombre.getText(),
                Float.parseFloat(precioporhora.getText()),
                tipo_masaje.getText(),
                Float.parseFloat(tiempo_exp.getText())
        ));
		btnGuardar.setBounds(316, 113, 89, 23);
		contentPane.add(btnGuardar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(actionEvent -> dispose());
		btnCancelar.setBounds(316, 181, 89, 23);
		contentPane.add(btnCancelar);
	}

}
