package ServiciosSpa.vista;

import ServiciosSpa.controlador.SistAdminServicios;
import ServiciosSpa.modelo.Servicio;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class BajaServicio extends JFrame {

	private JPanel contentPane;
	private JTextField codigo;
	private JTextField nombre;

	public BajaServicio() {
		setTitle("Eliminar Servicio");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Codigo");
		lblNewLabel.setBounds(73, 10, 108, 48);
		contentPane.add(lblNewLabel);
		
		codigo = new JTextField();
		codigo.setBounds(165, 21, 136, 25);
		contentPane.add(codigo);
		codigo.setColumns(10);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(actionEvent -> {
            SistAdminServicios.getInstancia().eliminarServicio(Long.parseLong(codigo.getText()));
            codigo.setText("");
        });
		btnBorrar.setBounds(73, 187, 108, 36);
		contentPane.add(btnBorrar);

		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(actionEvent -> {
            Servicio s = SistAdminServicios.getInstancia().buscarServicio(Long.parseLong(codigo.getText()));
            nombre.setText(s.getNombre());
        });
		btnBuscar.setBounds(336, 22, 89, 23);
		contentPane.add(btnBuscar);
		
		JButton btnCancela = new JButton("Cancelar");
		btnCancela.addActionListener(actionEvent -> dispose());
		btnCancela.setBounds(276, 187, 149, 36);
		contentPane.add(btnCancela);
		
		JLabel lblNewLabel_1 = new JLabel("Nombre");
		lblNewLabel_1.setBounds(73, 97, 82, 25);
		contentPane.add(lblNewLabel_1);
		
		nombre = new JTextField();
		nombre.setEditable(false);
		nombre.setBounds(165, 100, 136, 20);
		contentPane.add(nombre);
		nombre.setColumns(10);
	}
}
