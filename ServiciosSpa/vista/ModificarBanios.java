package ServiciosSpa.vista;

import ServiciosSpa.controlador.SistAdminServicios;
import ServiciosSpa.modelo.views.BanioView;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class ModificarBanios extends JFrame {

	private JPanel contentPane;
	private JTextField codigo;
	private JTextField precioxhora;
	private JTextField nombre;
	private JTextField elementos;
	private JTextField contraindicaciones;


	public ModificarBanios() {
		setTitle("Modificar Ba\u00F1o");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(36, 36, 97, 14);
		contentPane.add(lblCodigo);
		
		JLabel lblTipoDeServicio = new JLabel("Precio por hora");
		lblTipoDeServicio.setBounds(36, 81, 97, 14);
		contentPane.add(lblTipoDeServicio);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(36, 126, 97, 14);
		contentPane.add(lblNombre);
		
		JLabel lblElementosUtilizados = new JLabel("Elementos Utilizados");
		lblElementosUtilizados.setBounds(36, 171, 97, 14);
		contentPane.add(lblElementosUtilizados);
		
		JLabel lblContraindicaciones = new JLabel("Contraindicaciones");
		lblContraindicaciones.setBounds(36, 216, 97, 14);
		contentPane.add(lblContraindicaciones);
		
		codigo = new JTextField();
		codigo.setBounds(145, 33, 86, 20);
		contentPane.add(codigo);
		codigo.setColumns(10);
		
		precioxhora = new JTextField();
		precioxhora.setBounds(145, 78, 86, 20);
		contentPane.add(precioxhora);
		precioxhora.setColumns(10);
		
		nombre = new JTextField();
		nombre.setBounds(145, 123, 86, 20);
		contentPane.add(nombre);
		nombre.setColumns(10);
		
		elementos = new JTextField();
		elementos.setBounds(145, 168, 86, 20);
		contentPane.add(elementos);
		elementos.setColumns(10);
		
		contraindicaciones = new JTextField();
		contraindicaciones.setBounds(145, 213, 86, 20);
		contentPane.add(contraindicaciones);
		contraindicaciones.setColumns(10);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(actionEvent -> {
            BanioView b = SistAdminServicios.getInstancia().editarServicioBanio(Long.parseLong(codigo.getText()));
            nombre.setText(b.getNombre());
            precioxhora.setText(String.valueOf(b.getPrecioHora()));
            elementos.setText(b.getElementos());
            contraindicaciones.setText(b.getContraindicaciones());
        });
		btnBuscar.setBounds(241, 32, 89, 23);
		contentPane.add(btnBuscar);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(actionEvent -> SistAdminServicios.getInstancia().editarServicioBanio(
                Long.parseLong(codigo.getText()),
                nombre.getText(),
                Float.parseFloat(precioxhora.getText()),
                elementos.getText(),
                contraindicaciones.getText()
        ));
		btnGuardar.setBounds(293, 100, 89, 23);
		contentPane.add(btnGuardar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(actionEvent -> dispose());
		btnCancelar.setBounds(293, 167, 89, 23);
		contentPane.add(btnCancelar);
	}

}
