package ServiciosSpa.persistencia;

import ServiciosSpa.controlador.SistAdminServicios;
import ServiciosSpa.modelo.Paquete;
import ServiciosSpa.modelo.Servicio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AdmPersPaquete implements AdmPersServicio {

	private static AdmPersPaquete instancia;

	private AdmPersPaquete() {
		//instancia = new AdmPersPaquete();
	}

	public static AdmPersPaquete getInstancia() {
		if(instancia == null)
			instancia = new AdmPersPaquete();
		return instancia;
	}

	public void crear(Servicio s) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();
		Paquete sc = (Paquete) s;

		try {
			connection.setAutoCommit(false);
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO spa.paquete VALUES (?, ?, ?);"
			);

			statement.setLong(1, sc.getCodigo());
			statement.setString(2, sc.getNombre());
			statement.setFloat(3, sc.getDescuento());

			statement.execute();

			crearIndices(sc, connection); // creando tabla de indices

			connection.commit(); // commiteo la transaccion
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersPaquete: " + e.getMessage());
			System.err.println("[ERROR] AdmPersPaquete: " + e.getStackTrace());
			try {
				System.err.println("Rollback");
				connection.rollback();
			} catch(Exception e2) {
				System.err.println(e2.getMessage());
			}
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch(Exception e) {
				System.err.println("[ERROR] AdmPersPaquete: " + e.getMessage());
				System.err.println("[ERROR] AdmPersPaquete: " + e.getStackTrace());
			}
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}
	}

	private void crearIndices(Paquete sc, Connection connection) {
		for(Servicio s : sc.getServicios()) {
			try {
				PreparedStatement statement = connection.prepareStatement(
						"INSERT INTO spa.paquete_servicio (codigo_paquete, codigo_servicio) VALUES (?, ?);"
				);

				statement.setLong(1, sc.getCodigo());
				statement.setLong(2, s.getCodigo());

				statement.execute();
			} catch (Exception e) {
				System.err.println("[ERROR] AdmPersPaquete: " + e.getMessage());
				System.err.println("[ERROR] AdmPersPaquete: " + e.getStackTrace());
			}
		}
	}

	private void crearIndices(List<Servicio> agregar, long compuesto, Connection connection) {
		for(Servicio s : agregar) {
			try {
				PreparedStatement statement = connection.prepareStatement(
						"INSERT INTO spa.paquete_servicio (codigo_paquete, codigo_servicio) VALUES (?, ?);"
				);

				statement.setLong(1, compuesto);
				statement.setLong(2, s.getCodigo());

				statement.execute();
			} catch (Exception e) {
				System.err.println("[ERROR] AdmPersPaquete: " + e.getMessage());
				System.err.println("[ERROR] AdmPersPaquete: " + e.getStackTrace());
			}
		}
	}

	public List<Servicio> leerTodos() {
		Connection connection = PoolConnection.getPoolConnection().getConnection();
		List<Servicio> todos = new ArrayList<Servicio>();

		try {
			PreparedStatement statement = connection.prepareStatement(
					"SELECT * FROM spa.paquete;"
			);

			ResultSet resultado = statement.executeQuery();

			while(resultado.next()) {

				long codigoPaquete = resultado.getLong(1);

				List<Servicio> servicios = leerServicios(codigoPaquete);

				todos.add((Servicio) new Paquete(
						codigoPaquete,
						resultado.getString(2),
						resultado.getFloat(3),
						servicios,
						true
				));
			}
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersPaquete: " + e.getMessage());
			System.err.println("[ERROR] AdmPersPaquete: " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}

		return todos;
	}

	private List<Servicio> leerServicios(long compuesto) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();
		List<Servicio> todos = new ArrayList<Servicio>();

		try {
			PreparedStatement statement = connection.prepareStatement(
					"SELECT codigo_servicio FROM spa.paquete_servicio WHERE codigo_paquete = ?;"
			);

			statement.setLong(1, compuesto);

			ResultSet resultado = statement.executeQuery();

			while (resultado.next()) {
				todos.add(SistAdminServicios
								.getInstancia()
								.buscarServicio(resultado.getLong(1))
				);
			}
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersPaquete: " + e.getMessage());
			System.err.println("[ERROR] AdmPersPaquete: " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}

		return todos;
	}

	public Servicio leer(long codigo) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();
		Servicio uno = null;

		try {
			PreparedStatement statement = connection.prepareStatement(
					"SELECT * FROM spa.paquete WHERE codigo = ?;"
			);

			statement.setLong(1, codigo);

			ResultSet resultado = statement.executeQuery();

			while(resultado.next()) {

				long codigoPaquete = resultado.getLong(1);

				List<Servicio> servicios = leerServicios(codigoPaquete);

				uno= (Servicio) new Paquete(
						codigoPaquete,
						resultado.getString(2),
						resultado.getFloat(3),
						servicios,
						true
				);
			}
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersPaquete: " + e.getMessage());
			System.err.println("[ERROR] AdmPersPaquete: " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}

		return uno;
	}

	public void actualizar(Servicio s) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();
		Paquete sc = (Paquete) s;

		try {
			connection.setAutoCommit(false);

			PreparedStatement statement = connection.prepareStatement(
					"UPDATE TABLE spa.paquete SET nombre = ?, descuento = ? WHERE codigo = ?;"
			);

			statement.execute();

			// Leo todos los servicios de los que esta compuesto el SC que estoy actualizando
			List<Servicio> enBD = leerServicios(sc.getCodigo());
			// Creo una nueva lista de Servicios con los que estan en la BD
			List<Servicio> borrar = new ArrayList<Servicio>(enBD);
			// Borro los que ya no esten en memoria
			borrar.removeAll(sc.getServicios());

			if(borrar != null)
				eliminarServicios(borrar, connection);

			// Creo una nueva lista de Servicios con los que estan en memoria
			List<Servicio> agregar = new ArrayList<Servicio>(sc.getServicios());
			// Agrego los que no esten en la BD
			agregar.removeAll(enBD);

			if(agregar != null)
				crearIndices(agregar, sc.getCodigo(), connection);

			connection.commit();
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersPaquete: " + e.getMessage());
			System.err.println("[ERROR] AdmPersPaquete: " + e.getStackTrace());
			try {
				System.err.println("Rollback");
				connection.rollback();
			} catch(Exception e2) {
				System.err.println(e2.getMessage());
			}
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch(Exception e) {
				System.err.println("[ERROR] AdmPersPaquete: " + e.getMessage());
				System.err.println("[ERROR] AdmPersPaquete: " + e.getStackTrace());
			}
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}
	}

	private void eliminarServicios(List<Servicio> borrar, Connection connection) {
		for(Servicio s : borrar) {
			try {
				PreparedStatement statement = connection.prepareStatement(
						"DELETE FROM spa.paquete_servicio WHERE codigo_servicio = ?;"
				);

				statement.setLong(1, s.getCodigo());

				statement.execute();
			} catch (Exception e) {
				System.err.println("[ERROR] AdmPersPaquete: " + e.getMessage());
				System.err.println("[ERROR] AdmPersPaquete: " + e.getStackTrace());
			}
		}
	}

	private void eliminarServicios(Paquete sc, Connection connection) {
		for(Servicio s : sc.getServicios()) {
			try {
				PreparedStatement statement = connection.prepareStatement(
						"DELETE FROM spa.paquete_servicio WHERE codigo_paquete = ?;"
				);

				statement.setLong(1, sc.getCodigo());

				statement.execute();
			} catch (Exception e) {
				System.err.println("[ERROR] AdmPersPaquete: " + e.getMessage());
				System.err.println("[ERROR] AdmPersPaquete: " + e.getStackTrace());
			}
		}
	}

	public void eliminar(Servicio s) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();
		Paquete sc = (Paquete) s;

		try {
			connection.setAutoCommit(false);
			PreparedStatement statement = connection.prepareStatement(
					"DELETE FROM spa.paquete WHERE codigo = ?;"
			);

			statement.setLong(1, s.getCodigo());

			statement.execute();

			eliminarServicios(sc, connection);

			connection.commit();
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersPaquete: " + e.getMessage());
			System.err.println("[ERROR] AdmPersPaquete: " + e.getStackTrace());
			try {
				System.err.println("Rollback");
				connection.rollback();
			} catch(Exception e2) {
				System.err.println(e2.getMessage());
			}
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch(Exception e) {
				System.err.println("[ERROR] AdmPersPaquete: " + e.getMessage());
				System.err.println("[ERROR] AdmPersPaquete: " + e.getStackTrace());
			}
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}
	}

}
