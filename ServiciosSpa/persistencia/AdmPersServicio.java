package ServiciosSpa.persistencia;

import ServiciosSpa.modelo.Servicio;

import java.util.List;

public interface AdmPersServicio {

	void crear(Servicio s);

	List<Servicio> leerTodos();

	Servicio leer(long codigo);

	void actualizar(Servicio s);

	void eliminar(Servicio s);

}
