package ServiciosSpa.persistencia;

import ServiciosSpa.modelo.Drenaje;
import ServiciosSpa.modelo.Servicio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AdmPersDrenaje implements AdmPersServicio {

	private static AdmPersDrenaje instancia;

	private AdmPersDrenaje() {
		//instancia = new AdmPersDrenaje();
	}

	public static AdmPersDrenaje getInstancia() {
		if(instancia == null)
			instancia = new AdmPersDrenaje();
		return instancia;
	}

	public void crear(Servicio s) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();

		try {
			Drenaje d = (Drenaje) s;
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO spa.drenaje VALUES (?, ?, ?, ?, ?);"
			);

			statement.setLong(1, d.getCodigo());
			statement.setString(2, d.getNombre());
			statement.setFloat(3, d.getPrecioHora());
			statement.setString(4, d.getTipoDrenaje());
			statement.setString(5, d.getContraindicaciones());

			statement.execute();
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersDrenaje.crear(): " + e.getMessage());
			System.err.println("[ERROR] AdmPersDrenaje.crear(): " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}
	}

	public List<Servicio> leerTodos() {
		Connection connection = PoolConnection.getPoolConnection().getConnection();
		List<Servicio> todos = new ArrayList<Servicio>();

		try {
			PreparedStatement statement = connection.prepareStatement(
					"SELECT * FROM spa.drenaje;"
			);

			ResultSet resultado = statement.executeQuery();

			while(resultado.next()) {
				todos.add((Servicio) new Drenaje(
						resultado.getLong(1),
						resultado.getString(2),
						resultado.getFloat(3),
						resultado.getString(4),
						resultado.getString(5),
						true
				));
			}
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersDrenaje: " + e.getMessage());
			System.err.println("[ERROR] AdmPersDrenaje: " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}

		return todos;
	}

	public Servicio leer(long codigo) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();
		Servicio uno = null;

		try {
			PreparedStatement statement = connection.prepareStatement(
					"SELECT * FROM spa.drenaje WHERE codigo = ?;"
			);

			statement.setLong(1, codigo);
			statement.setFetchSize(1);

			ResultSet resultado = statement.executeQuery();

			while(resultado.next()) {
				uno = (Servicio) new Drenaje(
						resultado.getLong(1),
						resultado.getString(2),
						resultado.getFloat(3),
						resultado.getString(4),
						resultado.getString(5),
						true
				);
			}
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersDrenaje: " + e.getMessage());
			System.err.println("[ERROR] AdmPersDrenaje: " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}

		return uno;
	}

	public void actualizar(Servicio s) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();

		try {
			Drenaje d = (Drenaje) s;
			PreparedStatement statement = connection.prepareStatement(
					"UPDATE spa.drenaje SET nombre = ?," +
							"preciohora = ?," +
							"tipodrenaje = ?," +
							"contraindicaciones = ?" +
							"WHERE codigo = ?;"
			);

			statement.setString(1, d.getNombre());
			statement.setFloat(2, d.getPrecioHora());
			statement.setString(3, d.getTipoDrenaje());
			statement.setString(4, d.getContraindicaciones());
			statement.setLong(5, d.getCodigo());

			statement.execute();
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersDrenaje: " + e.getMessage());
			System.err.println("[ERROR] AdmPersDrenaje: " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}
	}

	public void eliminar(Servicio s) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();

		try {
			PreparedStatement statement = connection.prepareStatement(
					"DELETE FROM spa.drenaje WHERE codigo = ?;"
			);

			statement.setLong(1, s.getCodigo());

			statement.execute();
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersDrenaje: " + e.getMessage());
			System.err.println("[ERROR] AdmPersDrenaje: " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}
	}

}
