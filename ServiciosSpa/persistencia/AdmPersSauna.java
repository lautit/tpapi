package ServiciosSpa.persistencia;

import ServiciosSpa.modelo.Sauna;
import ServiciosSpa.modelo.Servicio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AdmPersSauna implements AdmPersServicio {

	private static AdmPersSauna instancia;

	public static AdmPersSauna getInstancia() {
		if(instancia == null)
			instancia = new AdmPersSauna();
		return instancia;
	}

	public void crear(Servicio s) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();

		try {
			Sauna sa = (Sauna) s;
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO spa.sauna VALUES (?, ?, ?, ?, ?);"
			);

			statement.setLong(1, sa.getCodigo());
			statement.setString(2, sa.getNombre());
			statement.setFloat(3, sa.getPrecioHora());
			statement.setFloat(4, sa.getTemperatura());
			statement.setFloat(5, sa.getTiempo());

			statement.execute();
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersSauna.crear(): " + e.getMessage());
			System.err.println("[ERROR] AdmPersSauna.crear(): " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}
	}

	public List<Servicio> leerTodos() {
		Connection connection = PoolConnection.getPoolConnection().getConnection();
		List<Servicio> todos = new ArrayList<Servicio>();

		try {
			PreparedStatement statement = connection.prepareStatement(
					"SELECT * FROM spa.sauna;"
			);

			ResultSet resultado = statement.executeQuery();

			while(resultado.next()) {
				todos.add((Servicio) new Sauna(
						resultado.getLong(1),
						resultado.getString(2),
						resultado.getFloat(3),
						resultado.getFloat(4),
						resultado.getFloat(5),
						true
				));
			}
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersSauna.leerTodos(): " + e.getMessage());
			System.err.println("[ERROR] AdmPersSauna.leerTodos(): " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}

		return todos;
	}

	public Servicio leer(long codigo) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();
		Servicio uno = null;

		try {
			PreparedStatement statement = connection.prepareStatement(
					"SELECT * FROM spa.sauna WHERE codigo = ?;"
			);

			statement.setLong(1, codigo);
			statement.setFetchSize(1);

			ResultSet resultado = statement.executeQuery();

			while(resultado.next()) {
				uno = (Servicio) new Sauna(
						resultado.getLong(1),
						resultado.getString(2),
						resultado.getFloat(3),
						resultado.getFloat(4),
						resultado.getFloat(5),
						true
				);
			}
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersSauna.leer(): " + e.getMessage());
			System.err.println("[ERROR] AdmPersSauna.leer(): " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}

		return uno;
	}

	public void actualizar(Servicio s) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();

		try {
			Sauna sa = (Sauna) s;
			PreparedStatement statement = connection.prepareStatement(
					"UPDATE spa.sauna SET nombre = ?," +
							"preciohora = ?," +
							"temperatura = ?," +
							"tiempo = ?" +
							"WHERE codigo = ?;"
			);

			statement.setString(1, sa.getNombre());
			statement.setFloat(2, sa.getPrecioHora());
			statement.setFloat(3, sa.getTemperatura());
			statement.setFloat(4, sa.getTiempo());
			statement.setLong(5, sa.getCodigo());

			statement.execute();
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersSauna.actualizar(): " + e.getMessage());
			System.err.println("[ERROR] AdmPersSauna.actualizar(): " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}
	}

	public void eliminar(Servicio s) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();

		try {
			PreparedStatement statement = connection.prepareStatement(
					"DELETE FROM spa.sauna WHERE codigo = ?;"
			);

			statement.setLong(1, s.getCodigo());

			statement.execute();
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersSauna.eliminar(): " + e.getMessage());
			System.err.println("[ERROR] AdmPersSauna.eliminar(): " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}
	}

}
