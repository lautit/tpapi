package ServiciosSpa.persistencia;

import ServiciosSpa.modelo.Banio;
import ServiciosSpa.modelo.Servicio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AdmPersBanio implements AdmPersServicio {

	private static AdmPersBanio instancia;

	private AdmPersBanio() {
		//instancia = new AdmPersBanio();
	}

	public static AdmPersBanio getInstancia() {
		if(instancia == null)
			instancia = new AdmPersBanio();
		return instancia;
	}

	public void crear(Servicio s) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();

		try {
			Banio b = (Banio) s;
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO spa.banio VALUES (?, ?, ?, ?, ?);"
			);

			statement.setLong(1, b.getCodigo());
			statement.setString(2, b.getNombre());
			statement.setFloat(3, b.getPrecioHora());
			statement.setString(4, b.getElementos());
			statement.setString(5, b.getContraindicaciones());

			statement.execute();
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersBanio.crear(): " + e.getMessage());
			System.err.println("[ERROR] AdmPersBanio.crear(): " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}
	}

	public List<Servicio> leerTodos() {
		Connection connection = PoolConnection.getPoolConnection().getConnection();
		List<Servicio> todos = new ArrayList<Servicio>();

		try {
			PreparedStatement statement = connection.prepareStatement(
					"SELECT * FROM spa.banio;"
			);

			ResultSet resultado = statement.executeQuery();

			while(resultado.next()) {
				todos.add((Servicio) new Banio(
						resultado.getLong(1),
						resultado.getString(2),
						resultado.getFloat(3),
						resultado.getString(4),
						resultado.getString(5),
						true
				));
			}
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersBanio.leerTodos(): " + e.getMessage());
			System.err.println("[ERROR] AdmPersBanio.leerTodos(): " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}

		return todos;
	}

	public Servicio leer(long codigo) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();
		Servicio uno = null;

		try {
			PreparedStatement statement = connection.prepareStatement(
					"SELECT * FROM spa.banio WHERE codigo = ?;"
			);

			statement.setLong(1, codigo);
			statement.setFetchSize(1);

			ResultSet resultado = statement.executeQuery();

			while(resultado.next()) {
				uno = (Servicio) new Banio(
						resultado.getLong(1),
						resultado.getString(2),
						resultado.getFloat(3),
						resultado.getString(4),
						resultado.getString(5),
						true
				);
			}
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersBanio.leer(): " + e.getMessage());
			System.err.println("[ERROR] AdmPersBanio.leer(): " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}

		return uno;
	}

	public void actualizar(Servicio s) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();

		try {
			Banio b = (Banio) s;
			PreparedStatement statement = connection.prepareStatement(
					"UPDATE spa.banio SET nombre = ?," +
							"preciohora = ?," +
							"elementos = ?," +
							"contraindicaciones = ?" +
							"WHERE codigo = ?;"
			);

			statement.setString(1, b.getNombre());
			statement.setFloat(2, b.getPrecioHora());
			statement.setString(3, b.getElementos());
			statement.setString(4, b.getContraindicaciones());
			statement.setLong(5, b.getCodigo());

			statement.execute();
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersBanio.actualizar(): " + e.getMessage());
			System.err.println("[ERROR] AdmPersBanio.actualizar(): " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}
	}

	public void eliminar(Servicio s) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();

		try {
			PreparedStatement statement = connection.prepareStatement(
					"DELETE FROM spa.banio WHERE codigo = ?;"
			);

			statement.setLong(1, s.getCodigo());

			statement.execute();
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersBanio.eliminar(): " + e.getMessage());
			System.err.println("[ERROR] AdmPersBanio.eliminar(): " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}
	}

}
