package ServiciosSpa.persistencia;

import ServiciosSpa.modelo.Masaje;
import ServiciosSpa.modelo.Servicio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AdmPersMasaje implements AdmPersServicio {

	private static AdmPersMasaje instancia;

	private AdmPersMasaje() {
		//instancia = new AdmPersMasaje();
	}

	public static AdmPersMasaje getInstancia() {
		if(instancia == null)
			instancia = new AdmPersMasaje();
		return instancia;
	}

	public void crear(Servicio s) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();

		try {
			Masaje m = (Masaje) s;
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO spa.masaje VALUES (?, ?, ?, ?, ?);"
			);

			statement.setLong(1, m.getCodigo());
			statement.setString(2, m.getNombre());
			statement.setFloat(3, m.getPrecioHora());
			statement.setString(4, m.getTipoMasaje());
			statement.setFloat(5, m.getTiempo());

			statement.execute();
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersMasaje: " + e.getMessage());
			System.err.println("[ERROR] AdmPersMasaje: " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}
	}

	public List<Servicio> leerTodos() {
		Connection connection = PoolConnection.getPoolConnection().getConnection();
		List<Servicio> todos = new ArrayList<Servicio>();

		try {
			PreparedStatement statement = connection.prepareStatement(
					"SELECT * FROM spa.masaje;"
			);

			ResultSet resultado = statement.executeQuery();

			while(resultado.next()) {
				todos.add((Servicio) new Masaje(
						resultado.getLong(1),
						resultado.getString(2),
						resultado.getFloat(3),
						resultado.getString(4),
						resultado.getFloat(5),
						true
				));
			}
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersMasaje: " + e.getMessage());
			System.err.println("[ERROR] AdmPersMasaje: " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}

		return todos;
	}

	public Servicio leer(long codigo) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();
		Servicio uno = null;

		try {
			PreparedStatement statement = connection.prepareStatement(
					"SELECT * FROM spa.masaje WHERE codigo = ?;"
			);

			statement.setLong(1, codigo);
			statement.setFetchSize(1);

			ResultSet resultado = statement.executeQuery();

			while(resultado.next()) {
				uno = (Servicio) new Masaje(
						resultado.getLong(1),
						resultado.getString(2),
						resultado.getFloat(3),
						resultado.getString(4),
						resultado.getFloat(5),
						true
				);
			}
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersMasaje: " + e.getMessage());
			System.err.println("[ERROR] AdmPersMasaje: " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}

		return uno;
	}

	public void actualizar(Servicio s) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();

		try {
			Masaje m = (Masaje) s;
			PreparedStatement statement = connection.prepareStatement(
					"UPDATE spa.masaje SET nombre = ?," +
							"preciohora = ?," +
							"tipomasaje = ?," +
							"tiempo = ?" +
							"WHERE codigo = ?;"
			);

			statement.setString(1, m.getNombre());
			statement.setFloat(2, m.getPrecioHora());
			statement.setString(3, m.getTipoMasaje());
			statement.setFloat(4, m.getTiempo());
			statement.setLong(5, m.getCodigo());

			statement.execute();
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersMasaje: " + e.getMessage());
			System.err.println("[ERROR] AdmPersMasaje: " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}
	}

	public void eliminar(Servicio s) {
		Connection connection = PoolConnection.getPoolConnection().getConnection();

		try {
			PreparedStatement statement = connection.prepareStatement(
					"DELETE FROM spa.masaje WHERE codigo = ?;"
			);

			statement.setLong(1, s.getCodigo());

			statement.execute();
		} catch (Exception e) {
			System.err.println("[ERROR] AdmPersMasaje: " + e.getMessage());
			System.err.println("[ERROR] AdmPersMasaje: " + e.getStackTrace());
		} finally {
			PoolConnection.getPoolConnection().releaseConnection(connection);
		}
	}

}
