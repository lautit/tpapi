package ServiciosSpa.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class GeneradorCodigo {

    private static GeneradorCodigo instancia;

    private long codigo;

    private GeneradorCodigo() {
        //instancia = new GeneradorCodigo();

        this.codigo = obtenerUltimo();
    }

    public static GeneradorCodigo getInstancia() {
        if(instancia == null)
            instancia = new GeneradorCodigo();
        return instancia;
    }

    private long obtenerUltimo() {
        Connection connection = PoolConnection.getPoolConnection().getConnection();
        long ultimo = 1;

        String select = "SELECT codigo, nombre FROM spa.";
        String union = " UNION SELECT codigo, nombre FROM spa.";

        try {
            String sql = "SELECT codigo FROM ("
                    + select + "sauna AS s"
                    + union + "masaje AS m"
                    + union + "banio AS b"
                    + union + "drenaje AS d"
                    + union + "paquete AS p" +
                    ") AS c ORDER BY codigo DESC LIMIT 1;";

            PreparedStatement statement = connection.prepareStatement(sql);

            ResultSet resultado = statement.executeQuery();

            while(resultado.next()) {
                ultimo = resultado.getLong("codigo");
            }
        } catch (Exception e) {
            System.err.println("[ERROR] GeneradorCodigo: " + e.getMessage());
            System.err.println("[ERROR] GeneradorCodigo: " + e.getStackTrace());
        } finally {
            PoolConnection.getPoolConnection().releaseConnection(connection);
        }

        return ultimo;
    }

    public long obtenerCodigo() {
        return codigo++;
    }
}
