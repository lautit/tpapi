package ServiciosSpa.controlador;

import ServiciosSpa.modelo.views.ServicioView;

import java.util.List;

public class ServicioREST {

	public float consultarPrecio(long servicio){
		return SistAdminServicios.getInstancia().consultarPrecio(servicio);
	}

	public List<ServicioView> listarServicios() {
		return SistAdminServicios.getInstancia().listarServicios();
	}

}
