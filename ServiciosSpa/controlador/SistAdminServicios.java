package ServiciosSpa.controlador;

import ServiciosSpa.modelo.*;
import ServiciosSpa.modelo.views.*;
import ServiciosSpa.persistencia.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class SistAdminServicios {

	private static SistAdminServicios instancia;
	private List<Servicio> servicios;
	//private List<AdmPersServicio> persistencias;
	private static GeneradorCodigo codigos;

	private SistAdminServicios() {
		//instancia = new SistAdminServicios();
		codigos = GeneradorCodigo.getInstancia();
		//this.persistencias = new LinkedList<AdmPersServicio>();

		//this.persistencias.add(AdmPersSauna.getInstancia());
		//this.persistencias.add(AdmPersDrenaje.getInstancia());
		//this.persistencias.add(AdmPersBanio.getInstancia());
		//this.persistencias.add(AdmPersMasaje.getInstancia());
		//this.persistencias.add(AdmPersPaquete.getInstancia());

		this.servicios = new ArrayList<Servicio>();

		/*persistencias.forEach((persistencia) -> {
			this.servicios.addAll(persistencia.leerTodos());
		});*/

		this.servicios.addAll(AdmPersSauna.getInstancia().leerTodos());
		this.servicios.addAll(AdmPersDrenaje.getInstancia().leerTodos());
		this.servicios.addAll(AdmPersBanio.getInstancia().leerTodos());
		this.servicios.addAll(AdmPersMasaje.getInstancia().leerTodos());
		this.servicios.addAll(AdmPersPaquete.getInstancia().leerTodos());
	}

	public static SistAdminServicios getInstancia() {
		if(instancia == null)
			instancia = new SistAdminServicios();
		return instancia;
	}

	public void crearServicioSauna(String nombre, float precioHora, float temperatura, float tiempoExposicion) {
		this.servicios.add(
				new Sauna(
						codigos.obtenerCodigo(),
						nombre,
						precioHora,
						temperatura, tiempoExposicion));
	}
	
	public void crearServicioMasajes(String nombre, float precioHora, String tipo, float tiempoExposicion) {
		this.servicios.add(
				new Masaje(
						codigos.obtenerCodigo(),
						nombre,
						precioHora,
						tipo, tiempoExposicion));
	}
	
	public void crearServicioBanios(String nombre, float precioHora, String elementos, String contraind) {
		this.servicios.add(
				new Banio(
						codigos.obtenerCodigo(),
						nombre,
						precioHora,
						elementos, contraind));
	}
	
	public void crearServicioDrenaje(String nombre, float precioHora, String tipo, String contraind) {
		this.servicios.add(
				new Drenaje(
						codigos.obtenerCodigo(),
						nombre,
						precioHora,
						tipo, contraind));
	}
	
	public void eliminarServicio(long codigo) {
		Servicio eliminar = buscarServicio(codigo);
		if (eliminar != null) {
			this.servicios.remove(eliminar);
			String tipo = eliminar.tipo();

			switch (tipo) {
				case "Sauna":
					AdmPersSauna.getInstancia().eliminar(eliminar);
				case "Drenaje":
					AdmPersDrenaje.getInstancia().eliminar(eliminar);
				case "Masaje":
					AdmPersMasaje.getInstancia().eliminar(eliminar);
				case "Banio":
					AdmPersBanio.getInstancia().eliminar(eliminar);
				case "Paquete":
					AdmPersPaquete.getInstancia().eliminar(eliminar);
			}
		}
	}

	public SaunaView editarServicioSauna(long codigo) {
		return ((Sauna) buscarServicio(codigo)).getView();
	}
	
	public DrenajeView editarServicioDrenaje(long codigo) {
		return ((Drenaje) buscarServicio(codigo)).getView();
	}

	public BanioView editarServicioBanio(long codigo) {
		return ((Banio) buscarServicio(codigo)).getView();
	}
	
	public MasajeView editarServicioMasaje(long codigo) {
		return ((Masaje) buscarServicio(codigo)).getView();
	}

	public PaqueteView editarServicioPaquete(long codigo) {
		return ((Paquete) buscarServicio(codigo)).getView();
	}
	
	public List<ServicioView> listarServicios(String tipo) {
		List<ServicioView> servicioViews = new ArrayList<ServicioView>();

		for(Servicio servicio : this.servicios) {
			if(servicio.tipo().equals(tipo)) {
				servicioViews.add(servicio.getView());
			}
		}

		return servicioViews;
	}

	public List<ServicioView> listarServicios() {
		List<ServicioView> servicioViews = new ArrayList<ServicioView>();

		for(Servicio servicio : this.servicios) {
			servicioViews.add(servicio.getView());
		}

		return servicioViews;
	}

	public Servicio buscarServicio(long codigo) {
		for(Servicio servicio : this.servicios) {
			if (servicio.sosServicio(codigo))
				return servicio;
		}

		return null;
	}

	public float consultarPrecio(long servicio) {
		Servicio buscar = buscarServicio(servicio);

		if(buscar == null)
			new Exception("No existe el servicio buscado");

		return buscar.calcularPrecio();
	}

	public void editarServicioBanio(long codigo, String nombre, float precioHora, String elementos, String contraindicaciones) {
		Banio b = (Banio) buscarServicio(codigo);

		b.setNombre(nombre);
		b.setPrecioHora(precioHora);
		b.setElementos(elementos);
		b.setContraindicaciones(contraindicaciones);

		AdmPersBanio.getInstancia().actualizar(b);
	}

	public void editarServicioDrenaje(long codigo, String nombre, float precioHora, String tipoDrenaje, String contraindicaciones) {
		Drenaje d = (Drenaje) buscarServicio(codigo);

		d.setNombre(nombre);
		d.setPrecioHora(precioHora);
		d.setTipoDrenaje(tipoDrenaje);
		d.setContraindicaciones(contraindicaciones);

		AdmPersDrenaje.getInstancia().actualizar(d);
	}

	public void editarServicioMasaje(long codigo, String nombre, float precioHora, String tipoMasaje, float tiempo) {
		Masaje m = (Masaje) buscarServicio(codigo);

		m.setNombre(nombre);
		m.setPrecioHora(precioHora);
		m.setTipoMasaje(tipoMasaje);
		m.setTiempo(tiempo);

		AdmPersMasaje.getInstancia().actualizar(m);
	}

	public void editarServicioSauna(long codigo, String nombre, float precioHora, float temperatura, float tiempo) {
		Sauna s = (Sauna) buscarServicio(codigo);

		s.setNombre(nombre);
		s.setPrecioHora(precioHora);
		s.setTemperatura(temperatura);
		s.setTiempo(tiempo);

		AdmPersSauna.getInstancia().actualizar(s);
	}

	public void editarServicioPaquete(long codigo, String nombre, float descuento, List<ServicioView> serviciosView) {
		Paquete p = (Paquete) buscarServicio(codigo);

		p.setNombre(nombre);
		p.setDescuento(descuento);

		List<Servicio> servicios = new ArrayList<Servicio>();

		for(ServicioView servicioView : serviciosView) {
			servicios.add(this.buscarServicio(servicioView.getCodigo()));
		}

		p.setServicios(servicios);

		AdmPersPaquete.getInstancia().actualizar(p);
	}

	public void crearServicioPaquete(String nombre, float descuento, List<ServicioView> serviciosView) {
		List<Servicio> servicios = new ArrayList<Servicio>();

		for(ServicioView servicioView : serviciosView) {
			servicios.add(this.buscarServicio(servicioView.getCodigo()));
		}

		this.servicios.add(
				new Paquete(
						codigos.obtenerCodigo(),
						nombre,
						descuento,
						servicios));
	}
}
